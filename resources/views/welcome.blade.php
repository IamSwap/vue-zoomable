<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div id="app">
            <zoomable src="http://www.w3schools.com/bootstrap/img_flower2.jpg"></zoomable>

            <zoomable src="http://www.w3schools.com/bootstrap/img_chania.jpg" ></zoomable>

            <zoomable src="http://www.w3schools.com/bootstrap/img_flower.jpg"></zoomable>

            <zoomable src="http://www.w3schools.com/bootstrap/img_flower2.jpg"></zoomable>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
